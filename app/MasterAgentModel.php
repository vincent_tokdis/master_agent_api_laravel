<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CodeModel;
use App\MasterAgentSupplierModel;

class MasterAgentModel extends BaseModel
{
    //
    protected $table = 'master_agent';
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
            [
                'full_name'         => 'required|string',
                'email_address'     => 'bail|required|email|unique:master_agent,email_address' . ($id ? ",$id" : ''),
                'avatar'            => 'required|string',
                'handphone'         => 'required|numeric',
                'ktp'               => 'required|numeric',
                'date_of_birth'     => 'required|date',
                'gender'            => 'required|string'
            ], 
            $merge);
    }
    public function codes(){
        return $this->hasMany(CodeModel::class, 'master_agent_id');
    }
    public function suppliers(){
        return $this->hasMany(MasterAgentSupplierModel::class, 'master_agent_id');
    }
    static function getCodes($id){
        $model_codes = parent::with('codes')->find($id);
        return $model_codes;
    }
    static function getSupplier($id){
        $model_supplier = parent::with('suppliers')->find($id);
        return $model_supplier;
    }
}

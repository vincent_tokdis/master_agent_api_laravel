<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterAgentModel;
use Validator;

class MasterAgentController extends Controller
{
    //
    public function index(){
        try {
            $master_agent_list = MasterAgentModel::getAll();
            return response()->json(array(
                'data'          => $master_agent_list,
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        } 
    }

    public function getByID($id){
        try {
            $master_agent_code = MasterAgentModel::getCodes($id);
            return response()->json(array(
                'data'          => $master_agent_code,
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        } 
    }

    public function save(Request $request){
        try {
            $params = $request->all();

            $validator = Validator::make($params, MasterAgentModel::rules());
            if( $validator->fails() ) {
                return response()->json(array(
                    'message' => $validator->errors(),
                    'status'  => 403
                ));
            }
            $master_agent_save = new MasterAgentModel();
            $master_agent_save->save($params);
            return response()->json(array(
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status'        => 500
            ));
        } 
    }

    public function update(Request $request, $id){
        try {
            $params = $request->all();
            
            $validator = Validator::make($params, MasterAgentModel::rules($id));
            if( $validator->fails() ) {
                return response()->json(array(
                    'message' => $validator->errors(),
                    'status'  => 403
                ));
            }
            $master_agent_update = new MasterAgentModel();
            
            $master_agent_update->edit($params, $id);
            return response()->json(array(
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status'        => 500
            ));
        } 
    }

    public function getBySupplier($id){
        try {
            $master_agent_supplier = MasterAgentModel::getSupplier($id);
            return response()->json(array(
                'data'          => $master_agent_supplier,
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        } 
    }
}

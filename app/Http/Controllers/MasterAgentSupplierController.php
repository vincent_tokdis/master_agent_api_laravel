<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterAgentSupplierModel;
use Validator;

class MasterAgentSupplierController extends Controller
{
    //
    public function index(Request $request){
        try {
            $params = $request->all();
            $master_agent_supplier_list = MasterAgentSupplierModel::getAllWithRank($params);
            return response()->json(array(
                'data'          => $master_agent_supplier_list,
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        } 
    }

    public function getByID($id){
        try {
            $master_agent_supplier = MasterAgentSupplierModel::getByID($id);
            return response()->json(array(
                'data'          => $master_agent_supplier,
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'data'          => [],
                'status'        => 500
            ));
        } 
    }

    public function save(Request $request){
        try {
            $params = $request->all();

            $validator = Validator::make($params, MasterAgentSupplierModel::rules());
            if( $validator->fails() ) {
                return response()->json(array(
                    'message' => $validator->errors(),
                    'status'  => 403
                ));
            }
            $master_agent_supplier_save = new MasterAgentSupplierModel();
            $master_agent_supplier_save->save($params);
            return response()->json(array(
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status'        => 500
            ));
        } 
    }

    public function update(Request $request, $id){
        try {
            $params = $request->all();
            
            $validator = Validator::make($params, MasterAgentSupplierModel::rules($id));
            if( $validator->fails() ) {
                return response()->json(array(
                    'message' => $validator->errors(),
                    'status'  => 403
                ));
            }
            $master_agent_supplier_update = new MasterAgentSupplierModel();
            
            $master_agent_supplier_update->edit($params, $id);
            return response()->json(array(
                'status'        => 200
            ));
        } catch (Exception $e) {
            return response()->json(array(
                'status'        => 500
            ));
        } 
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterAgentSupplierModel extends BaseModel
{
    //
    protected $table = 'master_agent_supplier';
    public static function rules ($id=0, $merge=[]){
        return array_merge(
            [
                'master_agent_id'       => 'required|numeric',
                'supplier_id'           => 'bail|required|numeric|unique:master_agent_supplier,supplier_id' . ($id ? ",$id" : ''),
                'supplier_code'         => 'bail|required|string|unique:master_agent_supplier,supplier_code' . ($id ? ",$id" : ''),
                'supplier_name'         => 'required|string',
                'supplier_percentage'   => 'required|numeric'
            ], 
            $merge);
    }
    public function master_agent(){
        return $this->belongsTo('App\MasterAgentModel');
    }
}

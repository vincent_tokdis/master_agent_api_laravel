<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeModel extends Model
{
    //
    protected $table = 'codes';
    public function master_agent(){
        return $this->belongsTo('App\MasterAgentModel');
    }
}

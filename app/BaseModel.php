<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BaseModel extends Model
{
    //
    public $order_column = 'created_at';
    public $order_value = 'desc';
    public $page = 0;
    public $offset = 10;
    public function __construct(){
        $this->order_column = 'created_at';
        $this->order_column = 'created_at';
    }
    static function getAll(){
        $model_list = parent::where('is_deleted','=',0)->get();
        return $model_list;
    }
    static function getAllWithRank(array $options = array()){
        if(!empty($options['order_column'])){
            $order_column = $options['order_column'];
        }
        if(!empty($options['order_value'])){
            $order_value = $options['order_value'];
        }
        if(!empty($options['page'])){
            $page = $options['page'];
        }
        if(!empty($options['offset'])){
            $offset = $options['offset'];
        }
        $model_list_rank = parent::selectRaw('*, row_number() over(order by id) as nomor_urut')
        ->where('is_deleted','=',0)
        ->orderBy($order_column, $order_value)
        ->skip($page * $offset)->take($offset)->get();
        return $model_list_rank;
    }    
    static function getByID($id){
        $model_single = parent::find($id)->first();
        return $model_single;
    }
    public function save(array $options = array()){
        $options['is_active']   = 1;
        $options['is_deleted']  = 0;
        $options['created_by']  = 1;
        $model_save = parent::insert($options);
    }
    public function edit(array $options = array(), $id){
        $options['updated_by']  = 1;
        $model_save = parent::where('id', '=', $id)->update($options);
    }
}

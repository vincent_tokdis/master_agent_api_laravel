<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//master_agent
Route::get('/master_agent','MasterAgentController@index');
Route::get('/master_agent/{id}','MasterAgentController@getByID');
Route::get('/master_agent_by_supplier/{id}','MasterAgentController@getBySupplier');
Route::post('/master_agent','MasterAgentController@save');
Route::put('/master_agent/{id}','MasterAgentController@update');

//master_agent_supplier
Route::get('/master_agent_supplier','MasterAgentSupplierController@index');
Route::get('/master_agent_supplier/{id}','MasterAgentSupplierController@getByID');
Route::post('/master_agent_supplier','MasterAgentSupplierController@save');
Route::put('/master_agent_supplier/{id}','MasterAgentSupplierController@update');